let array=[{"season":2008,"count":58},{"season":2009,"count":57},{"season":2010,"count":60},{"season":2011,"count":73},{"season":2012,"count":74},{"season":2013,"count":76},{"season":2014,"count":60},{"season":2015,"count":59},{"season":2016,"count":60},{"season":2017,"count":59}];
let arr1=[];
let arr2=[]
array.forEach(element => {
    arr1.push(element['season'])
    arr2.push(element['count'])
});
 Highcharts.chart('container', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Matches per Season'
    },
    subtitle: {
      text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
      categories:arr1,
      title: {
        text: null
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No Of Matches',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' '
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: '',
      align: '',
      verticalAlign: "",
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: arr2

    }]
});