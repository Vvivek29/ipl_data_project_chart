Highcharts.chart('container', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Matches win per year per team'
    },
    xAxis: {
      categories: ['Chennai Super Kings', 'Deccan Chargers', 'Delhi Daredevils', 'Gujarat Lions', 'Kings XI Punjab',
      'Kochi Tuskers Kerala','Kolkata Knight Riders','Mumbai Indians','Pune Warriors','Rajasthan Royals','Rising Pune Supergiant',
      'Royal Challengers Bangalore','Sunrisers Hyderabad']
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Matches win'
      }
    },
    legend: {
      reversed: true
    },
    plotOptions: {
      series: {
        stacking: 'normal'
      }
    },
    series: [{
      name: '2008',
      data: [9,2,7,0,10,0,6,7,0,13,0,4,0]
    }, {
      name: '2009',
      data: [8,9,10,0,7,0,3,5,0,6,0,9,0]
    }, {
      name: '2010',
      data: [9,8,7,0,4,0,7,11,0,6,0,8,0]
    }, {
        name: '2011',
        data: [11,6,4,0,7,6,8,10,4,6,0,10,0]
      }, {
        name: '2012',
        data: [10,4,11,0,8,0,12,10,4,7,0,8,0]
      }, {
        name: '2013',
        data: [12,0,3,0,8,0,6,13,4,11,0,9,10]
      },{
        name: '2014',
        data: [10,0,2,0,12,0,11,7,0,7,0,5,6]
      }, {
        name: '2015',
        data: [10,0,5,0,3,0,7,10,0,7,0,8,7]
      }, {
        name: '2016',
        data: [0,0,7,9,4,0,8,7,0,0,6,9,11]
      },{
        name: '2017',
        data: [0,0,6,4,7,0,9,12,0,0,10,3,8]
      }]
  });