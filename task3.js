let array=[ { bowling_team: 'Delhi Daredevils', extra_runs: '106' },
{ bowling_team: 'Gujarat Lions', extra_runs: '98' },
{ bowling_team: 'Kings XI Punjab', extra_runs: '100' },
{ bowling_team: 'Kolkata Knight Riders', extra_runs: '122' },
{ bowling_team: 'Mumbai Indians', extra_runs: '102' },
{ bowling_team: 'Rising Pune Supergiants', extra_runs: '108' },
{ bowling_team: 'Royal Challengers Bangalore',
  extra_runs: '156' },
{ bowling_team: 'Sunrisers Hyderabad', extra_runs: '107' } ]
let arr1=[];
let arr2=[]
array.forEach(element => {
    arr1.push(element['bowling_team'])
    arr2.push(+element['extra_runs'])
});
// console.log(arr1);
// console.log(arr2);
 Highcharts.chart('container', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Extra Runs per team in the year 2016'
    },
    subtitle: {
      text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
      categories:arr1,
      title: {
        text: null
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Extra Runs',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' '
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: '',
      align: '',
      verticalAlign: "",
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: arr2

    }]
});