let array=[[ 'RN ten Doeschate', 3.43 ],
[ 'J Yadav', 4.14 ],
[ 'V Kohli', 5.45 ],
[ 'R Ashwin', 5.72 ],
[ 'S Nadeem', 5.86 ],
[ 'Z Khan', 6.15 ],
[ 'Parvez Rasool', 6.2 ],
[ 'MC Henriques', 6.27 ],
[ 'MA Starc', 6.75 ],
[ 'M de Lange', 6.92 ]]
let arr1=[];
let arr2=[]
for(var i=0;i<array.length;i++){
    arr1.push(array[i][0])
    arr2.push(+array[i][1])
}
// console.log(arr1);
//  console.log(arr2);
 Highcharts.chart('container', {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Best Economical Bowler in the year 2015'
    },
    subtitle: {
      text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
      categories:arr1,
      title: {
        text: null
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Economy rate',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' '
    },
    plotOptions: {
      bar: {
        dataLabels: {
          enabled: true
        }
      }
    },
    legend: {
      layout: '',
      align: '',
      verticalAlign: "",
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: [{
      name: '',
      data: arr2

    }]
});